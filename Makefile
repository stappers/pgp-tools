-include /usr/share/dpkg/pkg-info.mk

DIRS=caff gpg-key2ps gpg-mailkeys gpgsigs gpglist gpgparticipants keyanalyze keylookup \
     sig2dot springgraph gpgwrap gpgdir keyart gpg-key2latex
TGZ=../signing-party_$(DEB_VERSION_UPSREAM).orig.tar.gz
TGZ_DIR=signing-party-$(DEB_VERSION_UPSREAM)

all:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir || exit 1 ; fi ; done

install:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir install || exit 1 ; fi ; done

clean:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir clean || exit 1 ; fi ; done

dist:
	[ -d debian ] && fakeroot debian/rules clean
	[ ! -f $(TGZ) ]
	mkdir $(TGZ_DIR)
	for dir in $(DIRS) ; do cp -a $$dir $(TGZ_DIR); done
	cp -a README Makefile $(TGZ_DIR)
	GZIP=--best tar cvz -f $(TGZ) --exclude .svn $(TGZ_DIR)
	rm -rf $(TGZ_DIR)

tag-release:
	if svn ls svn+ssh://svn.debian.org/svn/pgp-tools/tags/release-$(DEB_VERSION_UPSREAM) >/dev/null 2>&1; then \
		echo "Already exists." >&2; exit 1; \
	fi
	svn cp -m 'tagging release $(DEB_VERSION_UPSREAM)' svn+ssh://svn.debian.org/svn/pgp-tools/trunk svn+ssh://svn.debian.org/svn/pgp-tools/tags/release-$(DEB_VERSION_UPSREAM)

tag-debian-version:
	if svn ls svn+ssh://svn.debian.org/svn/pgp-tools/tags/debian-version-$(DEB_VERSION) >/dev/null 2>&1; then \
		echo "Already exists." >&2; exit 1; \
	fi
	svn cp -m 'tagging debian version $(DEB_VERSION)' svn+ssh://svn.debian.org/svn/pgp-tools/trunk svn+ssh://svn.debian.org/svn/pgp-tools/tags/debian-version-$(DEB_VERSION)
